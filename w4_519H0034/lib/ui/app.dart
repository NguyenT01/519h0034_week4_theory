import 'package:flutter/material.dart';

class App extends StatelessWidget {
  const App({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Week4',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Week4'),
        ),
        body: Home(),
      ),

    );
  }
}

class Home extends StatefulWidget {
  Home({Key? key}) : super(key: key);

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final formKey = GlobalKey<FormState>();
  String email = "";
  String password = "";

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Container(
        padding: EdgeInsets.all(16),
        child: Form(
          key: formKey,
          child: Column(
            children: [
              emailForm(),
              const SizedBox(height: 16,),
              psdForm(),
              const SizedBox(height: 16,),
              btnSubmit()
            ],
          ),
        ),
      ),
    );
  }

  Widget emailForm()
  {
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'Your email',
        icon: Icon(Icons.email),
      ),
      keyboardType: TextInputType.emailAddress,
      validator: (text){
        if(text == null){
          return "Please fill in your email";
        }
        else if(!text.contains('@')){
          return "It is not look like a validate email address. Please try again.";
        }
        return null;
      },
      onSaved: (datas){
        email = datas!;
      },
    );
  }

  Widget psdForm(){
    return TextFormField(
      decoration: InputDecoration(
        hintText: 'Enter your password',
        icon: Icon(Icons.password),
      ),
      keyboardType: TextInputType.text,
      obscureText: true,
      onSaved: (datas){
        password = datas!;
      },
      validator: (text){
        if(text==null){
          return "Please enter your password";
        }
        else if(text.length <8 ){
          return "Password contains at least 8 letters or more.";
        }
        else if(!text.contains(RegExp(r'[A-Z]'))){
          return "Password need at least 1 uppercase letter.";
        }
        else if(!text.contains(RegExp('[a-z]'))){
          return "Password need at least 1 lower letter.";
        }
        else if (!text.contains(RegExp('[!@#\$&*~\<\>?\,\.\/]'))){
          return "Password need at least 1 special letter.";
        }
        return null;
      },
    );
  }

  Widget btnSubmit(){
    return ElevatedButton(
      child: Text(
        'SUBMIT',
        style: TextStyle(fontWeight: FontWeight.bold),
      ),
      onPressed: (){
        if(formKey.currentState!.validate()){
          formKey.currentState!.save();
          print(email);
          print(password);
        }
      },
    );
  }

}
